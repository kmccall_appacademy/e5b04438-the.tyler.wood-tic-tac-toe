class Board
  attr_accessor :grid

  def initialize(grid_input = nil)
    if grid_input
      @grid = grid_input
    else
      @grid = Array.new(3) { Array.new(3) } if @grid == nil
    end
  end

  def [](pos)
    row, col = pos
    @grid[row][col]
  end

  def []=(pos, mark)
    row, col = pos
    @grid[row][col] = mark
  end

  def place_mark(pos,mark)
    self[pos] = mark
  end

  def empty?(pos)
    self[pos].nil?
  end

  def over?
    if @grid.all? { |row| row.none? { |pos| pos.nil? } }
      true
    elsif self.winner == nil
      return false
    elsif !self.winner.nil?
      return self.winner
    end
  end

  def winner
    for i in 0..2
      row_positions = [[i,0],[i,1],[i,2]]
      if [self[row_positions[0]],
          self[row_positions[1]],
          self[row_positions[2]]
         ].all? { |item| item == :X}
        return :X
      elsif [self[row_positions[0]],
             self[row_positions[1]],
             self[row_positions[2]]
           ].all? { |item| item == :O}
        return :O
      end

      if [self[row_positions[0].reverse],
          self[row_positions[1].reverse],
          self[row_positions[2].reverse]
         ].all? { |item| item == :X}
        return :X
      elsif [self[row_positions[0].reverse],
             self[row_positions[1].reverse],
             self[row_positions[2].reverse]
           ].all? { |item| item == :O}
        return :O
      end

    end
    left_diagonal_positions =  [[2,0],[1,1],[0,2]]
    right_diagonal_positions = [[0,0],[1,1],[2,2]]
    if left_diagonal_positions.all? { |pos| self[pos] == :X}
      return :X
    elsif right_diagonal_positions.all?{ |pos| self[pos] == :O}
      return :O
    elsif left_diagonal_positions.all? { |pos| self[pos] == :O}
        return :O
    elsif right_diagonal_positions.all?{ |pos| self[pos] == :X}
        return :X
    end
    nil
  end

end
