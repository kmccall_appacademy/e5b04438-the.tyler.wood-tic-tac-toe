class ComputerPlayer
  attr_accessor :name, :board, :mark

  def initialize(name)
    @name = name
  end

  def display(input)
    @board = input
  end

  def get_move
    for i in 0..2
      for j in 0..2
        @board.place_mark([i,j],self.mark)
        if @board.winner
          return [j,i]
        end
      end
    end

    row = rand(2)
    col = rand(2)
    [col,row]

  end

end
