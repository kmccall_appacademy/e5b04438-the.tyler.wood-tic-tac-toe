class HumanPlayer
  attr_accessor :name
  def initialize(name)
    @name = name
  end

  def get_move
    puts "Please enter your move in the format 'X, Y'"
    inputs = gets.chomp
    return inputs.split(", ").map{ |x| x.to_i }
  end

  def display(board)
    puts board.grid
  end

end
