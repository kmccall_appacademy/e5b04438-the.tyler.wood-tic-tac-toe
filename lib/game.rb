require_relative 'board'
require_relative 'human_player'
require_relative 'computer_player'

class Game
  attr_accessor :board, :p1, :p2, :current_player

  def initialize(p1, p2)
    @board = Board.new
    @p1 = p1
    @p2 = p2
    @p1.mark,@p2.mark = :X,:O
    @current_player = @p1
  end

  def play_turn
    if @current_player == @p1
      @board.place_mark(@current_player.get_move, @current_player.mark)
      self.switch_players!
    else
      @board.place_mark(@p2.get_move, @p2.mark)
      self.switch_players!
    end
  end

  def switch_players!
    if @current_player == @p1
      @current_player = @p2
    else
      @current_player = @p1
    end
  end

end
